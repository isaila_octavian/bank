package model;
import java.io.Serializable;
import java.util.Observable;

public class Account implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int number;
	Person titular;
	float amount;
	String type;
	public Account(int number, Person titular, float amount,String type) {
		this.number=number;
		this.titular=titular;
		this.amount=amount;
		this.type=type;
	}
	public int getNumber() {
		return this.number;
	}
	public String getType() {
		return this.type;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public float getAmount() {
		return this.amount;
	}
	public void setAmount(float number) {
		this.amount = number;
	}
	public void deposit(float f) {
		this.amount=this.amount+f;
	}
	public void withdraw(float amount2) {
		if (this.amount>=amount2) {
			this.amount=this.amount-amount2;
		}
		else this.amount=0;
	}
	public String toString() {
		String re;
		re="Contul: [numar= "+number+", titular= "+titular+", suma= "+amount+", tip= "+type+"]";
		return re;
	}

	   
}
