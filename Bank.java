package model;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

public class Bank implements BankProc, Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String FILENAME = "banc.ser";
	public HashMap<Person, ArrayList<Account>> accounts;
	public Bank() {
		this.accounts = new HashMap<Person,ArrayList<Account>>();
		this.read();
	}
	public boolean isWellFormed() {
		for (Person p: accounts.keySet()) {
			if (p.id <0) {
				return false;
			}
			if(p.phone.length()!=10 && (p.phone.charAt(0)!=0 && p.phone.charAt(1)!=7)) {
				return false;
			}
			for (Account a: accounts.get(p)) {
				if (a.number<0) return false;
				if(a.amount<0) return false;
			}
		}
		return true;
	}

	public void addPerson(Person i) {
		assert (i!=null) : "Error add person: person null";
		assert isWellFormed();
		accounts.put(i, new ArrayList<Account>());
		assert accounts.containsKey(i);
		assert isWellFormed();
		
	}
	
	public void removePerson(Person i) {
		assert (i!=null) : "Error add person: person null";
		assert isWellFormed();
		accounts.remove(i,accounts.get(i));
		assert !accounts.containsKey(i);
		assert isWellFormed();
	}
	
	public void addAccount(Person p,Account i) {
		assert (i!=null) : "Error add account: account null";
		assert(p!=null): "Error: person null"; 
		assert isWellFormed();
		assert accounts.containsKey(p) : "This person doesn't exists in the bank!";
		System.out.println(i.toString());
		
		accounts.get(p).add(i);
		assert accounts.get(p).contains(i);
		assert isWellFormed();
		
	}
	
	public void removeAccount(Person p, Account i) {
		assert (i!=null) : "Error add account: account null";
		assert(p!=null): "Error: person null"; 
		assert isWellFormed();
		assert accounts.get(p).contains(i) : "The account doesn't belong to anyone";
		assert accounts.containsKey(p) : "This person doesn't exists in the bank!";
		accounts.get(p).remove(i);
		assert !accounts.get(p).contains(i);
		assert isWellFormed();
	}
	
	public void  read() {
		HashMap<Person,ArrayList<Account>> data = null;
		try{
			FileInputStream file = new FileInputStream(FILENAME);
			ObjectInputStream in = new ObjectInputStream(file);
			data = (HashMap<Person,ArrayList<Account>>)in.readObject();
			in.close();
			file.close();
		}catch(IOException | ClassNotFoundException e1){
			e1.printStackTrace();
		}
		
		accounts = data;
	}
	
	public void write() {
		try{
			FileOutputStream file = new FileOutputStream(FILENAME);
			ObjectOutputStream out = new ObjectOutputStream(file);
			out.writeObject(accounts);
			out.close();
			file.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	@Override
	public void report() {
		// TODO Auto-generated method stub
		
	}
}
