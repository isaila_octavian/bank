package model;
public interface BankProc {
	public void addPerson(Person i);
	
	public void removePerson(Person i);
	
	public void addAccount(Person p, Account i);
	
	public void removeAccount(Person p, Account i);
	
	public void  read();
	
	public void write();
	
	public void report();
	
}
