package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.html.HTMLDocument.Iterator;

import model.Account;
import model.Bank;
import model.Person;
import model.SavingAccount;
import model.SpendingAccount;
import view.ViewAccount;
import view.ViewPerson;

public class Controller{

	private ViewPerson view1;
	private ViewAccount view2;
	private Bank bank=new Bank();
	private Listener btnListener;
	
	public Controller(ViewPerson view1, ViewAccount view2) {
		this.view1 = view1;
		//this.bank=bank;
		bank.read();
		this.view2=view2;
		btnListener = new Listener(this);
		view1.addBtnListener(btnListener);
		view2.addBtnListener(btnListener);
	}
	 public static <T> JTable createTable(List<T> objects) throws IllegalArgumentException, IllegalAccessException {
         ArrayList<String> columnNamesArrayList = new ArrayList<String>(); 
         for(Field field : objects.get(0).getClass().getDeclaredFields()) {
             field.setAccessible(true);
             columnNamesArrayList.add(field.getName());
         }
         String[] columnNames = new String[columnNamesArrayList.size()];
         columnNames = columnNamesArrayList.toArray(columnNames);
         DefaultTableModel tableModel = new DefaultTableModel(columnNames, 0);
         java.util.Iterator<T> i = objects.iterator();
         while(i.hasNext()) {
             Object object = i.next();
             ArrayList<Object> columnDataAsArrayList = new ArrayList<Object>();
             for(Field field : object.getClass().getDeclaredFields()) {
                 field.setAccessible(true);
                 columnDataAsArrayList.add(field.get(object));
             }
             Object[] columnDataAsArray = new Object[columnDataAsArrayList.size()];
             columnDataAsArray = columnDataAsArrayList.toArray(columnDataAsArray);
             tableModel.addRow(columnDataAsArray);
         }
         JTable table = new JTable(tableModel);
         return table;
     }

	public class Listener implements ActionListener {
		private Controller controller;
		
		public Listener(Controller controller) {
			this.controller = controller;
		}
		@Override
		public void actionPerformed(ActionEvent arg0) {
			// TODO Auto-generated method stub
			if(arg0.getActionCommand()=="listperson" ) {
				List<Object> list = new ArrayList<Object>();
				for (Person p : bank.accounts.keySet()) {
					list.add(p);
					System.out.println(p.toString());
				}
				try {
					view1.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(arg0.getActionCommand()=="insertP") {
				int id=Integer.parseInt(view1.textId.getText());
				String name=view1.textName.getText();
				String phone=view1.textPhone.getText();
				Person p=new Person(id, name, phone);
				bank.addPerson(p);
				view1.showMessage("Person inserted!");
				List<Object> list = new ArrayList<Object>();
				for (Person p1 : bank.accounts.keySet()) {
					list.add(p1);
					System.out.println(p1.toString());
				}
				try {
					view1.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bank.write();
				
			}
			else if(arg0.getActionCommand()=="deleteP") {
				int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
				String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
				String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
				Person p=new Person(id1,nume, tele);
				System.out.println(p);
				System.out.println("-----"+p.toString());
				bank.removePerson(p);
				view1.showMessage("Person removed!");
				List<Object> list = new ArrayList<Object>();
				for (Person p1 : bank.accounts.keySet()) {
					list.add(p1);
					System.out.println(p1.toString());
				}
				try {
					view1.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				List<Object> list1= new ArrayList<Object>();
				for (Person p1: bank.accounts.keySet()) { 
					for (Account aa: bank.accounts.get(p1)) {
						list1.add(new Account(aa.getNumber(),p1,aa.getAmount(),aa.getType()));
					}
				}
				try {
					view2.setTable(createTable(list1));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bank.write();
			}
			else if (arg0.getActionCommand()=="updateP") {
				int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
				String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
				String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
				Person p=new Person(id1,nume, tele);
				int idNou=Integer.parseInt(view1.textId.getText());
				String numeNou=view1.textName.getText();
				String telnou=view1.textPhone.getText();
				bank.accounts.put(new Person(idNou,numeNou, telnou), bank.accounts.get(p));
				bank.removePerson(p);
				view1.showMessage("Person updated!");
				List<Object> list = new ArrayList<Object>();
				for (Person p1 : bank.accounts.keySet()) {
					list.add(p1);
					System.out.println(p1.toString());
				}
				try {
					view1.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bank.write();
			}
			
			else if(arg0.getActionCommand()=="listacc") {
				List<Object> list= new ArrayList<Object>();
				for (Person p: bank.accounts.keySet()) {
					for (Account a: bank.accounts.get(p)) {
						list.add(new Account(a.getNumber(),p,a.getAmount(),a.getType()));
					}
				}
				try {
					view2.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (arg0.getActionCommand()=="insertacc") {
		
				int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
				String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
				String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
				Person p=new Person(id1,nume, tele);
				if (view2.strategy.getSelectedItem().toString().equals("Saving Account")) {
					 float amount=Float.parseFloat(view2.textAmount.getText());
					 float interest = Float.parseFloat(view2.textInterest.getText());
					// int number=Integer.parseInt(view2.textNumber.getText());
					Account a= new SavingAccount(bank.accounts.get(p).size() + 1,p,amount,"saving",interest);
					System.out.println(a.toString());
						bank.addAccount(p, a);
						view2.showMessage("Saving account successfully added to " + p.toString() + "!"); 
						
				 }	 
				else if (view2.strategy.getSelectedItem().toString().equals("Spending Account")) {
					float amount=Float.parseFloat(view2.textAmount.getText());
					//int number=Integer.parseInt(view2.textNumber.getText());
					Account a=new SpendingAccount(bank.accounts.get(p).size() + 1,p,amount,"spending");
					bank.addAccount(p, a);
					view2.showMessage("Spending account successfully added to " + p.toString() + "!"); 
					
				}
				List<Object> list= new ArrayList<Object>();
				for (Person p1: bank.accounts.keySet()) { 
					for (Account a: bank.accounts.get(p1)) {
						list.add(new Account(a.getNumber(),p1,a.getAmount(),a.getType()));
					}
				}
				bank.write();
				try {
					view2.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if (arg0.getActionCommand()=="deleteacc") {
			
				int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
				String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
				String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
				Person p=new Person(id1,nume, tele);
				int number=Integer.parseInt(view2.getTable().getValueAt(view2.getTable().getSelectedRow(), 0).toString());
				bank.removeAccount(p, bank.accounts.get(p).get(number-1));
				int num = 1;
				for (Account a : bank.accounts.get(p)) {
					a.setNumber(num);
					num++;
				}
				view2.showMessage("Account deleted");
				List<Object> list= new ArrayList<Object>();
				for (Person p1: bank.accounts.keySet()) { 
					for (Account a: bank.accounts.get(p1)) {
						list.add(new Account(a.getNumber(),p1,a.getAmount(),a.getType()));
					}
				}
				try {
					view2.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bank.write();
				
			}
			else if (arg0.getActionCommand()=="updateacc") {
			
				int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
				String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
				String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
				Person p=new Person(id1,nume, tele);
				int number=Integer.parseInt(view2.getTable().getValueAt(view2.getTable().getSelectedRow(), 0).toString());
				float amount=Float.parseFloat(view2.textAmount.getText());
				bank.accounts.get(p).get(number-1).setAmount(amount);
				view2.showMessage("Account updated!");
				List<Object> list= new ArrayList<Object>();
				for (Person p1: bank.accounts.keySet()) { 
					for (Account a: bank.accounts.get(p1)) {
						list.add(new Account(a.getNumber(),p1,a.getAmount(),a.getType()));
					}
				}
				try {
					view2.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bank.write();
			}
			else if(arg0.getActionCommand()=="deposit") {
				String type=view2.getTable().getValueAt(view2.getTable().getSelectedRow(), 3).toString(); 
				if (type.equals("saving")) {
					view2.showMessage("You can deposit money in your saving account just once!");
				}
				else if (type.equals("spending")) {
					try {
						int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
						String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
						String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
						Person p=new Person(id1,nume, tele);
						System.out.println(p);
						float amount1 = Float.parseFloat(view2.textAmount.getText());
						int number = Integer.parseInt(view2.getTable().getValueAt(view2.getTable().getSelectedRow(), 0).toString());
						bank.accounts.get(p).get(number-1).deposit(amount1);
						view2.showMessage("Money deposited!");
				} catch (NumberFormatException ex) {
					view2.showMessage("Invalid data!");
				}
						List<Object> list= new ArrayList<Object>();
						for (Person p1: bank.accounts.keySet()) { 
							for (Account aa: bank.accounts.get(p1)) {
								list.add(new Account(aa.getNumber(),p1,aa.getAmount(),aa.getType()));
							}
						}
						try {
							view2.setTable(createTable(list));
						} catch (IllegalArgumentException | IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						bank.write();
				
				
			}}
			else if(arg0.getActionCommand()=="withdraw") {
				float money = 0;
				String tip=view2.getTable().getValueAt(view2.getTable().getSelectedRow(), 3).toString(); 
				int id1=Integer.parseInt(view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 0).toString());
				String nume=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 1).toString();
				String tele=view1.getTable().getValueAt(view1.getTable().getSelectedRow(), 2).toString();
				Person p=new Person(id1,nume, tele);
				int number=Integer.parseInt(view2.getTable().getValueAt(view2.getTable().getSelectedRow(),0).toString());
				float amount=Float.parseFloat(view2.getTable().getValueAt(view2.getTable().getSelectedRow(),2).toString());
				if (tip.equals("saving")) {
					float dobanda=Float.parseFloat(view2.textInterest.getText());
					int years=Integer.parseInt(view2.textYears.getText());
					for (int i=1;i<=years;i++) {
						 money+=amount*dobanda;
						 amount+=money;
					}
					bank.accounts.get(p).get(number-1).withdraw(amount);
					view2.showMessage("An amount of "+amount+" was withdrawn");
				}
				else if(tip.equals("spending")) {
					float am=Float.parseFloat(view2.textAmount.getText());
					bank.accounts.get(p).get(number-1).withdraw(am);
					view2.showMessage("An amount of "+am+" was withdrawn");
				}
				List<Object> list= new ArrayList<Object>();
				for (Person p1: bank.accounts.keySet()) { 
					for (Account aa: bank.accounts.get(p1)) {
						list.add(new Account(aa.getNumber(),p1,aa.getAmount(),aa.getType()));
					}
				}
				try {
					view2.setTable(createTable(list));
				} catch (IllegalArgumentException | IllegalAccessException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bank.write();
			}
			
			
		}
	}
	
}
