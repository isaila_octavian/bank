package model;
import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

public class Person implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int id;
	String name;
	String phone;
	
	public Person(int id, String name, String phone) {
		this.id=id;
		this.name=name;
		this.phone=phone;
	}
	public int getId() {
		return this.id;
	}
	public String getName() {
		return this.name;
	}
	public String getPhone() {
		return this.phone;
	}
	
	
	public String toString() {
		String re;
		re="Person "+id+": "+name;
		return re;
	}
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + id;
	    result = prime * result + name.hashCode();
	    result=prime*result+phone.hashCode();
	    return result;
	}
	public boolean equals(Object obj) {
	    if (this == obj)
	        return true;
	    if (obj == null)
	        return false;
	    if (getClass() != obj.getClass())
	        return false;
	    Person other = (Person) obj;
	    return (this.id==other.id && this.name.equals(other.name) && this.phone.equals(other.phone));
	}
}
