package model;

public class SavingAccount extends Account{
	private float dobanda;
	public SavingAccount(int number, Person titular, float amount,String type, float dobanda) {
		super(number, titular, amount,type);
		// TODO Auto-generated constructor stub
		this.dobanda=dobanda;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public void withdraw(int years) {
		
		for (int i=1; i<=years; i++) {
			super.deposit(amount*dobanda); 
		}
		
	}
	public String toString() {
		String re;
		re="Contul: [numar= "+number+", titular= "+titular+", suma= "+amount+"], de tipul "+ type +", cu dobanda: "+dobanda;
		return re;
	}
}
