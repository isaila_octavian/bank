package model;

public class SpendingAccount extends Account{

	public SpendingAccount(int number, Person titular,float amount,String type) {
		super(number, titular, amount,type);
		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public void deposit(float amount) {
		super.deposit(amount);
	}
	public void withdraw(float amount) {
		super.withdraw(amount);
	}
	public String toString() {
		String re;
		re="Contul: [numar= "+number+", titular= "+titular+", suma= "+amount+"], de tipul "+type;
		return re;
	}
}
