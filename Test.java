package test;

import static org.junit.Assert.*;

import model.Account;
import model.Bank;
import model.Person;
import model.SpendingAccount;

public class Test {
	@org.junit.Test
	public void testAddClient() {
		Bank bank = new Bank();
		Person p = new Person(5, "Ana","0744674125");
		bank.addPerson(p);
		bank.write();
		assertTrue(bank.accounts.containsKey(p));
	}
	@org.junit.Test
	public void testAddAccount() {
		Bank bank=new Bank();
		Person p = new Person(5, "Ana","0744674125");
		Account a=new SpendingAccount(1,p,2500,"spending");
		bank.addAccount(p, a);
		assertTrue(bank.accounts.get(p).contains(a));
	}
	@org.junit.Test
	public void removePerson() {
		Bank bank=new Bank();
		Person p=new Person(5, "Ana","0744674125");
		bank.removePerson(p);
		assertFalse(bank.accounts.containsKey(p));
	}
	@org.junit.Test 
	public void testDeposit(){
		Bank bank=new Bank();
		Person p = new Person(5, "Ana","0744674125");
		Account a=new SpendingAccount(1,p,2500,"spending");
		bank.addAccount(p,a);
		a.deposit(100);
		assertTrue(a.getAmount()==2600);

	}
	
}
