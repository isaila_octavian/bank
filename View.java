package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import model.Bank;

public class View extends JComponent{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	protected static JFrame frame=new JFrame();
	JPanel pan=new JPanel();
	ViewPerson v1;
	ViewAccount v2;
	 JButton person=new JButton("Person");
	 JButton account=new JButton("Account");
	 JLabel labell=new JLabel("Find informations about: ");
	 public View() {

		 frame=new JFrame("Bank");
			frame.setBounds(100, 100, 500, 300);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			pan.setLayout(null);
			frame.setContentPane(pan);
			frame.setResizable(false);
			person.setBounds(150, 50, 150, 50);
			frame.getContentPane().add(person);
			account.setBounds(150, 130, 150, 50);
			frame.getContentPane().add(account);
			labell.setBounds(150,0,150,50);
			frame.getContentPane().add(labell);
			person.addActionListener(new Personn());
			account.addActionListener(new Acc());
			frame.setVisible(true);
	 }
	 class Personn implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	        	  v1=new ViewPerson();
	        }
	    }
	 class Acc implements ActionListener {
	        public void actionPerformed(ActionEvent e) {
	        	  v2=new ViewAccount();
	        }
	    }
}
