package view;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class ViewAccount  extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JPanel pae=new JPanel();
	public JTextField textInterest=new JTextField();
	public JTextField textAmount=new JTextField();
	public JTextField textYears=new JTextField();
	public JTextField textNumber=new JTextField();
	JLabel interest=new JLabel("Interest: ");
	JLabel amount=new JLabel("Amount: ");
	JLabel years=new JLabel("Years: ");
	JLabel acc=new JLabel("Pick the account type: ");
	JLabel accNo=new JLabel("Account number: ");
	private JButton btnInsert=new JButton("Add");
	private JButton btnDelete=new JButton("Remove");
	private JButton btnList1=new JButton("List");
	private JButton btnUpdate=new JButton("Update");
	private JButton withdraw=new JButton("Withdraw");
	private JButton deposit=new JButton("Deposit");
	 private JTable table;
	 private JScrollPane scrollPane=new JScrollPane();
	 String[] strategies= {"Saving Account","Spending Account"};
	 public JComboBox strategy=new JComboBox(strategies);
	public ViewAccount() {
		setTitle("Account frame");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 530, 500);
		pae = new JPanel();
		pae.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pae);
		pae.setLayout(null);
		amount.setBounds(65, 90, 100, 30);
		pae.add(amount);
		textAmount.setBounds(120, 90, 100, 30);
		pae.add(textAmount);
		accNo.setBounds(235, 90, 100, 30);
		pae.add(accNo);
		interest.setBounds(260, 10, 100,30);
		pae.add(interest);
		years.setBounds(390, 10, 100,30);
		pae.add(years);
		textInterest.setBounds(240, 40, 100, 30);
		pae.add(textInterest);
		textYears.setBounds(370, 40, 100, 30);
		pae.add(textYears);
		textNumber.setBounds(340, 90, 100, 30);
		pae.add(textNumber);
		scrollPane.setBounds(10, 250, 490, 160);
		pae.add(scrollPane);
		acc.setBounds(60, 10, 150, 30);
		pae.add(acc);
		strategy.setBounds(50,40,150,30);
		pae.add(strategy);
		btnInsert.setBounds(30, 180, 100, 30);
		pae.add(btnInsert);
		btnInsert.setActionCommand("insertacc");
		btnDelete.setBounds(150, 180, 100, 30);
		pae.add(btnDelete);
		btnDelete.setActionCommand("deleteacc");
		btnList1.setBounds(270, 180, 100, 30);
		pae.add(btnList1);
		btnList1.setActionCommand("listacc");
		btnUpdate.setBounds(390, 180, 100, 30);
		pae.add(btnUpdate);
		btnUpdate.setActionCommand("updateacc");
		withdraw.setBounds(150, 130, 100, 30);
		pae.add(withdraw);
		withdraw.setActionCommand("withdraw");
		deposit.setBounds(270, 130, 100, 30);
		pae.add(deposit);
		deposit.setActionCommand("deposit");
		this.setVisible(true);
	
	}
	 public void addBtnListener(ActionListener listener) {
			btnInsert.addActionListener(listener);
			btnDelete.addActionListener(listener);
			btnList1.addActionListener(listener);
			btnUpdate.addActionListener(listener);
			deposit.addActionListener(listener);
			withdraw.addActionListener(listener);
		}
	 public void setTable(JTable newTable) {
			this.table = newTable;
			scrollPane.setViewportView(table);
			repaint();
			revalidate();
		}
	 public JTable getTable() {
			return table;
		}
	 public void showMessage(String message) {
			JOptionPane.showMessageDialog(this, message);
		}
}
