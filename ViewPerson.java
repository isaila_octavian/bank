package view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import model.Bank;

public class ViewPerson extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  JPanel pae=new JPanel();
	public JTextField textId=new JTextField();
	public JTextField textName=new JTextField();
	public JTextField textPhone=new JTextField();
	JLabel name=new JLabel("Person name: ");
	JLabel id=new JLabel("Person Id: ");
	JLabel phone=new JLabel("Person phone number: ");
	private JButton btnInsert=new JButton("Insert");
	private JButton btnDelete=new JButton("Delete");
	private JButton btnList=new JButton("List");
	private JButton btnUpdate=new JButton("Update");
	 private JTable table;
	 private JScrollPane scrollPane=new JScrollPane();
	public ViewPerson() {
		setTitle("Person frame");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 530, 500);
		pae = new JPanel();
		pae.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pae);
		pae.setLayout(null);
		scrollPane.setBounds(10, 250, 490, 160);
		pae.add(scrollPane);
		id.setBounds(130, 10, 150, 50);
		pae.add(id);
		textId.setBounds(230, 20, 150, 30);
		pae.add(textId);
		name.setBounds(130, 50, 150, 50);
		pae.add(name);
		textName.setBounds(230, 60, 150, 30);
		pae.add(textName);
		phone.setBounds(90, 90, 150, 50);
		pae.add(phone);
		textPhone.setBounds(230, 100, 150, 30);
		pae.add(textPhone);
		btnInsert.setBounds(30, 180, 100, 30);
		btnInsert.setActionCommand("insertP");
		pae.add(btnInsert);
		btnDelete.setBounds(150, 180, 100, 30);
		btnDelete.setActionCommand("deleteP");
		pae.add(btnDelete);
		btnList.setBounds(270, 180, 100, 30);
		btnList.setActionCommand("listperson");
		pae.add(btnList);
		btnUpdate.setBounds(390, 180, 100, 30);
		btnUpdate.setActionCommand("updateP");
		pae.add(btnUpdate);
		this.setVisible(true);
	
	}
	
    

	 public void addBtnListener(ActionListener listener) {
			btnInsert.addActionListener(listener);
			btnDelete.addActionListener(listener);
			btnList.addActionListener(listener);
			btnUpdate.addActionListener(listener);
		}
	 public void setTable(JTable newTable) {
			this.table = newTable;
			scrollPane.setViewportView(table);
			repaint();
			revalidate();
		}
	 public JTable getTable() {
			return table;
		}
	 public void showMessage(String message) {
			JOptionPane.showMessageDialog(this, message);
		}
	
}
